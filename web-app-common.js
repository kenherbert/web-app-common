function formatDate(date) {
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    return `${months[date.getMonth()]} ${date.getDate()} ${date.getFullYear()}`;
}


function populateChangelog($changelog, $templateContainer, $lastUpdated, changelogData) {
    $changelog.empty();

    changelogData.forEach((entry, index) => {
        const $template = $templateContainer.find('.card').clone();
        const entryDate = new Date(entry.date);
        const items = entry.items.map(item => `<li>${item}</li>`);

        $template.find('.card-header').attr('id', `heading-${entry.date}`);
        $template.find('.card-header .btn').attr('data-target', `#collapse-${entry.date}`)
            .attr('aria-controls', `collapse-${entry.date}`)
            .text(formatDate(entryDate));

        $template.find('.collapse').attr('id', `collapse-${entry.date}`)
            .attr('aria-labelledby', `heading-${entry.date}`);
        $template.find('.collapse ul').html(items);

        if(index === 0) {
            $lastUpdated.text(entry.date);
            $template.find('.card-header .btn').removeClass('collapsed').attr('aria-expanded', 'true');
            $template.find('.collapse').addClass('show');
        }

        $changelog.append($template);
    });
}